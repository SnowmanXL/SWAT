package nl.kza.swat.browser;

import io.qameta.allure.Allure;
import io.qameta.allure.Step;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.nio.file.Files.newInputStream;
import static nl.kza.swat.browser.SharedDriver.getOriginalHandle;

public class BrowserInteractionImpl implements IBrowserActions {
    public WebDriver webDriver;
    private final Logger LOGGER = LoggerFactory.getLogger(BrowserInteractionImpl.class);
    private int retryCount = 0;

    public BrowserInteractionImpl() {
        this.webDriver = SharedDriver.getBrowserInstance();
    }

    @Step
    private void takeScreenshot(WebDriver driver) {
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        String fileName = "target/screenshots/screenshot_" + timeStamp + ".jpg";
        LOGGER.debug("Created Screenshot: {}", fileName);

        try {
            FileUtils.copyFile(scrFile, new File(fileName));
            Allure.addAttachment("Screenshot", newInputStream(Paths.get(fileName)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Step
    public void closeExtraTabs() {
        for (String handle : webDriver.getWindowHandles()) {
            if (!handle.equals(getOriginalHandle())) {
                webDriver.switchTo().window(handle);
                webDriver.close();
            }
        }
        webDriver.switchTo().window(getOriginalHandle());
    }

    @Step
    public void clickOn(WebElement element) {
        LOGGER.debug("click element {} ", element);
        if (isElementPresent(element))
            element.click();

        //manual reset of retry timer just in case it messes up the instances
        if (retryCount != 0) {
            retryCount = 0;
        }
    }

    @Step
    public void clearTextFrom(WebElement element) {
        LOGGER.debug("Clear text from field {}", element);
        if (isElementPresent(element)) {
            element.clear();
        } else {
            LOGGER.warn("Element {} not found, start waiting for element, screenshot has been taken", element);
            takeScreenshot(webDriver);
            waitForElement(element);
            element.clear();
        }

    }

    @Step
    public void fillTextIn(WebElement element, String keyText) {
        LOGGER.debug("Fill field {} with value {}", element, keyText);
        if (isElementPresent(element)) {
            element.sendKeys(keyText);
        } else {
            LOGGER.warn("Element {} not found, start waiting for element to send text {}, screenshot has been taken", element, keyText);
            takeScreenshot(webDriver);
            waitAndSendKeys(element, keyText);
        }
    }

    @Step
    public String getTextFrom(WebElement element) {
        LOGGER.debug("Get text from element {}", element);
        if (isElementPresent(element)) {
            return element.getText();
        } else {
            LOGGER.warn("Element {} not found, retrying for 20 seconds", element);
            takeScreenshot(webDriver);
            waitForElement(element);
            return element.getText();
        }
    }

    @Step
    public boolean textFromElementIsEqualTo(WebElement element, String compareValue) {
        LOGGER.debug("Compare {} text with compare string {}", compareValue);
        String foundText = getTextFrom(element);
        LOGGER.debug("Found text: {} , compare text: {}",foundText, compareValue);
        return foundText.equals(compareValue);
    }


    @Step
    private boolean isElementPresent(WebElement element) {
        LOGGER.debug("validate if element exist: {} ",element);

        try {
            element.getTagName();
            return true;
        } catch (NoSuchElementException e) {
            ++retryCount;
            LOGGER.error(" No such element found screenshot has been taken, starting to wait for element \n retrycount:" + retryCount, e);
            takeScreenshot(webDriver);
            if (retryCount != 1) {
                waitForElement(element);
                isElementPresent(element);
            }
            return false;
        }
    }

    @Step
    public void scrollTo(WebElement el) {
        if (webDriver instanceof JavascriptExecutor) {
            ((JavascriptExecutor) webDriver).executeScript("arguments[0].scrollIntoView(true);", el);
        }
    }


    @Step
    private void waitAndSendKeys(WebElement element, String text) {
        LOGGER.debug("Waiting for {} and sending text: {} to element", element, text);
        waitForElement(element);
        element.sendKeys(text);
    }

    @Step
    private void waitForElement(WebElement element) {
        LOGGER.debug("Start to wait for {} for 20 seconds ", element);
        WebDriverWait wait = new WebDriverWait(webDriver, 20, 500);
        wait.until(ExpectedConditions.visibilityOf(element));
    }


    @Step
    public void waitExplicitlyFor(int millis) {
        try {
            Thread.sleep(millis);
        } catch (final InterruptedException e) {
            LOGGER.error("Explicit wait interrupted", e);
        }
    }

    @Override
    public void navigateTo(String url) {
        LOGGER.debug("Navigate to {} ", url);
        webDriver.navigate().to(url);
    }

    @Override
    public WebDriver getWebDriver() {
        return webDriver;
    }
}

	