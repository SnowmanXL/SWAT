package nl.kza.swat.browser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 */
public interface IBrowserActions {
    /**
     *
     * @return
     */
    WebDriver getWebDriver();

    /**
     *
     * @param element
     */
    void clickOn(WebElement element);

    /**
     *
     */
    void closeExtraTabs();

    /**
     *
     * @param element
     */
    void clearTextFrom(WebElement element);

    /**
     *
     * @param element
     * @param keyText
     */
    void fillTextIn(WebElement element, String keyText);

    /**
     *
     * @param element
     * @return
     */
    String getTextFrom(WebElement element);

    /**
     *
     * @param element
     * @param compareValue
     * @return
     */
    boolean textFromElementIsEqualTo(WebElement element, String compareValue);

    /**
     *
     * @param el
     */
    void scrollTo(WebElement el);

    /**
     *
     * @param millis
     */
    void waitExplicitlyFor(int millis);

    void navigateTo(String url);
}
