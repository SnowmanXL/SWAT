package nl.kza.swat.browser;

import nl.kza.swat.util.TestConfig;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class BrowserFactory {

    public static WebDriver getBrowser() {
        String desiredBrowserName = System.getProperty("browser", TestConfig.valueFor("UseBrowser"));
        WebDriver desiredBrowser = null;

        try {
            switch (desiredBrowserName) {
                case "ie":
                    desiredBrowser = IEBrowser.buildIEBrowser();
                    break;
                case "chrome":
                    desiredBrowser = ChromeBrowser.buildChromeBrowser();
                    break;
                case "firefox":
                    desiredBrowser = FirefoxBrowser.buildFirefoxBrowser();
                    break;
                case "remote":
                    desiredBrowser = new RemoteBrowser().getDriver();
                default:
                    //TODO:work out what to do when a browser isn't needed
                    break;
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        return desiredBrowser;
    }

    private static class ChromeBrowser extends ChromeDriver {
        static WebDriver buildChromeBrowser() throws Throwable {
            System.setProperty("webdriver.chrome.driver", TestConfig.valueFor("WebDriverChromeDriverPath"));
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--start-maximized");
            ChromeBrowser browser = new ChromeBrowser(options);
            browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            return browser;
        }

        private ChromeBrowser(ChromeOptions options) {
            super(options);
        }
    }

    private static class FirefoxBrowser extends FirefoxDriver {

        static FirefoxBrowser buildFirefoxBrowser() throws Throwable {
            System.setProperty("webdriver.gecko.driver", TestConfig.valueFor("WebDriverFirefoxDriverPath"));
            FirefoxProfile ffprofile = new ProfilesIni().getProfile("Selenium");
            ffprofile.setAcceptUntrustedCertificates(true);

            FirefoxBrowser browser = new FirefoxBrowser(ffprofile);
            browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            return browser;
        }

        private FirefoxBrowser(FirefoxProfile desiredProfile) {
            super(desiredProfile);
        }
    }

    private static class IEBrowser extends InternetExplorerDriver {
        static WebDriver buildIEBrowser() throws Throwable {
            DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
            capabilities.setCapability(IGNORE_ZOOM_SETTING, true);

            System.setProperty("webdriver.ie.driver", TestConfig.valueFor("WebDriverIeDriverPath"));
            IEBrowser browser = new IEBrowser(capabilities);
            browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            return browser;
        }

        private IEBrowser(DesiredCapabilities capabilities) {
            super(capabilities);
        }
    }

    private static class RemoteBrowser {
        WebDriver driver;

        private RemoteBrowser() {
            System.setProperty("webdriver.chrome.driver", TestConfig.valueFor("WebDriverChromeDriverPath"));
            DesiredCapabilities capability = DesiredCapabilities.chrome();
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--start-maximized");
            capability.setCapability(ChromeOptions.CAPABILITY, options);
            try {
                driver = new RemoteWebDriver(new URL(TestConfig.valueFor("RemoteBrowserHubAddress")), capability);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        WebDriver getDriver() {
            return driver;
        }
    }
}
