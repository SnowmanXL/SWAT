package nl.kza.swat.basepages;

import nl.kza.swat.browser.IBrowserActions;
import org.openqa.selenium.support.PageFactory;

public abstract class BasePage {

    protected final IBrowserActions browser;

    public BasePage(IBrowserActions browser) {
        this.browser = browser;
        PageFactory.initElements(browser.getWebDriver(),this);
    }

}
