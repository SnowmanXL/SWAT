package nl.kza.swat.pages;

import nl.kza.swat.basepages.BasePage;
import nl.kza.swat.browser.IBrowserActions;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class KzaExpertisePage extends BasePage {

    public KzaExpertisePage(IBrowserActions browser) {
        super(browser);
    }

    @FindBy(id = "start")
    public WebElement template;
}
