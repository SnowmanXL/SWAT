package nl.kza.swat.pages;

import nl.kza.swat.basepages.BasePage;
import nl.kza.swat.browser.IBrowserActions;
import nl.kza.swat.util.TestConfig;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class KzaMainPage extends BasePage {

    public KzaMainPage(IBrowserActions browser) {
        super(browser);
    }

    @FindBy(xpath = "//a[text()='KZA Expertise']")
    public WebElement expertiseButton;

    public KzaMainPage openPage() {
        browser.navigateTo(TestConfig.valueFor("StartURL"));
        return this;
    }

    public KzaExpertisePage clickOnExpertise() {
        browser.clickOn(expertiseButton);
        return new KzaExpertisePage(browser);
    }


}
