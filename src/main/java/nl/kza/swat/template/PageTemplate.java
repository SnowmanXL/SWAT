package nl.kza.swat.template;

import nl.kza.swat.basepages.BasePage;
import nl.kza.swat.browser.IBrowserActions;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class PageTemplate extends BasePage {

	 @FindBy(id = "start") 		
	 public WebElement template;

	 @FindBy(css = "ul.dropdown-menu > li > a")
	 public List<WebElement> listTemplate;
	
	public PageTemplate(IBrowserActions actions) {
		super(actions);
	}

	public PageTemplate templateMethode(){
		browser.clickOn(template);
		return this;
	}



}
