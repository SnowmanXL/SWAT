package nl.kza.swat.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;

public class TestConfig {
    private static TestConfig testConfig;
    private static String requiredEnvironmentName;
    private static Properties properties;

    public static String valueFor(final String keyName){
        return getInstance().getProperty(keyName);
    }

    private static TestConfig getInstance(){
        if (testConfig == null) {
            properties = new Properties();
            requiredEnvironmentName = System.getProperty("env", "local");
            populateCommonProperties();
            populateEnvProperties(requiredEnvironmentName);
            testConfig = new TestConfig();
        }
        return testConfig;
    }

    private static void populateCommonProperties(){
        readInPropertiesFile("common");
    }

    private static void populateEnvProperties(final String requiredEnvironment){
        readInPropertiesFile(requiredEnvironment);
    }

    private static void readInPropertiesFile(String filePath){
        String propertiesFilePath = String.format("src/test/resources/config/common.properties", filePath);
        File propertiesFile = new File(propertiesFilePath);
        try {
            if(!propertiesFile.exists()) {
                throw new FileNotFoundException(
                        String.format("No properties file found at: %s", filePath));
            }
            InputStream input = new FileInputStream(propertiesFilePath);
            properties.load(input);
            input.close();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    private String getProperty(final String keyName) {
        String value = properties.getProperty(keyName);
        if(value == null) {
            throw new Error(String.format("Key %s not configured for environment %s", keyName, requiredEnvironmentName));
        }
        return value;
    }
}
