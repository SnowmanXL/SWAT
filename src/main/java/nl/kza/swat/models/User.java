package nl.kza.swat.models;

/**
 * Created by Wluijk on 7/18/2017.
 */
public class User {
    private String name;

    public User(String name) {
        this.name = name;
    }

    //Stub with default values in case the yml loading fails.
    public User(){
        this("Henk");

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}