package nl.kza.swat.tests;

import io.qameta.allure.*;
import io.qameta.allure.junit4.DisplayName;
import nl.kza.swat.browser.BrowserInteractionImpl;
import nl.kza.swat.browser.IBrowserActions;
import nl.kza.swat.models.User;
import nl.kza.swat.pages.KzaMainPage;
import nl.kza.swat.util.YamlLoader;
import org.junit.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Tester {

    private static final Logger LOGGER = LoggerFactory.getLogger(Tester.class);
    private static User user;
    private static YamlLoader yamlLoader = new YamlLoader();

    private IBrowserActions browserActions = new BrowserInteractionImpl();
    private KzaMainPage kzaMainPage;

    @Epic("Epic_name")
    @Feature("Feature_name")
    @BeforeClass
    public static void prepareTestSuite() {
        //Add log entry to mark the start of this test
        LOGGER.info("Starting TestClass Tester");

        //load config from yaml files
        user = yamlLoader.CreateUserFromYaml("testgebruiker");
    }

    @Before
    public void prepareTestCase() {
        kzaMainPage = new KzaMainPage(browserActions);
    }

    @Test
    @Issue("Number")
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Display_name")
    @Description("Description")
    public void InloggenCollega() {
        kzaMainPage.openPage()
                .clickOnExpertise();
    }

    @After
    public void finishTestCase() {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            LOGGER.error("Thread interrupted", e);
        }

        //TODO: Put this in a shiny method
        browserActions.getWebDriver().manage().deleteAllCookies();
    }

    @AfterClass
    public static void finishTestSuite() {

    }
}